// Config
// Note: Used `git update-index --assume-unchanged config.js` to ignore changes

var args  = require('argh').argv,
    debug = require('debug')('ts:config')

var config = {

	// Database
	db: process.env.DATABASE_URL || 'postgres://localhost/tsapp',

	// Secrets
	keys: {
		cookie : 'Secret',
		session: 'Secret'
	},

	// Heroku
	production: process.env.NODE_ENV == 'production',

	// Auth
	noAuth    : false,
	maxLogins : 5,
	saltRounds: 10,

	showInfo: 2
}

// Process arguments
debug('Args:', Object.keys(args).length)

for (var arg in args)
	config[arg] = args[arg]

module.exports = config
