// Database cache

/*	Stores data from subject, course, and teacher that will update rarely but
	may be queried often.

	{	subject: [ ... ],
		teacher: [ ... ],
		course : {
			'FDPC': 'Foundations and Pre-Calculus'
			...
		}
	}
*/

var db    = require('./db'),
    debug = require('debug')('ts:cache')

require('./titlecase') // .title()

var cacheDB = {},
    lengths = {},
    list = ['subject', 'teacher'] // course is handled seperately

exports.init = function(callback) {
	var queries = []

	list.forEach(function(table) {
		queries.push(
			db[table.title()]
				.findAndCountAll({ attributes: ['name'] })
				.then(function(docs) {
					var array = []
					docs.rows.forEach(function(inst) {
						array.push(inst.get('name'))
					})
					cacheDB[table] = array
					lengths[table] = docs.count
				})
		)
	})

	cacheDB.course = {}
	queries.push(
		db.Course
			.findAndCountAll({ attributes: ['id', 'name'] })
			.then(function(docs) {
				docs.rows.forEach(function(inst) {
					cacheDB.course[inst.get('id')] = inst.get('name')
				})
				lengths.course = docs.count
			})
	)

	Promise.all(queries).then(function() {
		debug('Cached:', lengths)
		if (callback) return callback()
		return
	})
}

exports.db = cacheDB

exports.insert = function(what, type) {
	if (type == 'course') // what will be a course object (id, name)
		cacheDB[type][what.id] = what.name
	else
		cacheDB[type].push(what)
	lengths[type]++
}

exports.remove = function(what, type) {
	if (type == 'course') // what will be a course id
		delete cacheDB[type][what]
	else
		cacheDB[type].splice(cacheDB[type].indexOf(what), 1)
	lengths[type]--
}

exports.exists = function(what, type) {
	// Recursively call itself if passed an array
	if (type.constructor === Array)
		return type.some(function(elem) { return exports.exists(what, elem) })

	if (type == 'course') {
		for (var id in cacheDB.course) {
			if (what.toUpperCase() == id || what == cacheDB.course[id])
				return true
		}
	} else return cacheDB[type].indexOf(what) > -1

	return false // Catch all
}
