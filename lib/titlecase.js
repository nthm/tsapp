// Prototype to convert strings to titlecase

String.prototype.title = function() {
	var small = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i
	return this.replace(/\b\w+/g, function(match, index) {
		// Don't convert a small word to lowercase if it's the first word
		if (index > 0 && match.search(small) > -1)
			return match.toLowerCase()
		return match.charAt(0).toUpperCase() + match.substr(1).toLowerCase()
	})
}
