// Validate POST data

var debug = require('debug')('ts:validate')
require('./titlecase') // .title()

var regexes = {
	name : /^[a-zA-Z -]{2,30}$/,
	pass : /^\S{2,30}/,
	id   : /^[a-zA-Z]{2,6}$/,
	year : /^(?:\d{2}){1,2}$/, //http://stackoverflow.com/questions/8177143/
	grade: /^\d{2}$/
}

var simplifiedNames = {
	password: ['pass', 'newPass', 'confirm']
}

var exists = function(haystack, needle) {
	return haystack.indexOf(needle) > -1
}

var allFields = [
	'name',
	'pass', 'newPass', 'confirm',
	'teacher', 'subject', 'course', 'id', 'year', 'grade', 'classlist', 'regex'
]

var optionalFields = ['regex']

module.exports = function(data) {
	debug('Validating')

	for (var field in data) {
		if (!exists(allFields, field))
			return 'Invalid form'

		// Skip the id of a class form since it has both an id and course id
		if (field == 'id' && data['course'] != undefined) continue

		var value = data[field],
		    name  = field
		// Simplify names to avoid responses like 'NewPassword left blank'
		for (var key in simplifiedNames) {
			if (exists(simplifiedNames[key], field)) {
				name = key
				break
			}
		}

		if (value.length == 0 && !exists(optionalFields, field))
			return name.title() + ' left blank'

		if (exists(Object.keys(regexes), field) && !regexes[field].exec(value))
			return 'Invalid ' + name

		if (field == 'confirm' && value != (data.newPass || data.pass))
			return 'Passwords don\'t match'
	}
}
