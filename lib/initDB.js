// Rebuild the test database

var db = require('./db')

var students = [],
    rand = function(max, min) {
    	return Math.floor(Math.random() * (max - min) + min)
    }

console.log('Rebuilding DB')
db.sync({
	force: true  // Drop all
})
.then(function() {
	console.log('Done: Sync\nTeachers')
	return db.Teacher.bulkCreate([
		{ name: 'Dev'   , pass: 'Dev'   },
		{ name: 'Alice' , pass: 'Alice' },
		{ name: 'Bob'   , pass: 'Bob'   },
	])
})
.then(function() {
	console.log('Done: Teachers\nSubjects')
	return db.Subject.bulkCreate([
		{ name: 'Math'    },
		{ name: 'Science' },
		{ name: 'Misc'    }
	])
})
.then(function() {
	console.log('Done: Subjects\nCourses')
	return db.Course.bulkCreate([
		{
			id: 'FDPC',
			name: 'Foundations and Pre-Calculus',
			subject: 'Math'
		}, {
			id: 'CALC',
			name: 'Calculus',
			subject: 'Math'
		}, {
			id: 'PC',
			name: 'Pre-Calculus',
			subject: 'Math'
		}, {
			id: 'PHYS',
			name: 'Physics',
			subject: 'Science'
		}, {
			id: 'IT',
			name: 'Information Technology',
			subject: 'Misc'
		}
	])
})
.then(function() {
	console.log('Done: Courses\nClasses')
	return db.Class.bulkCreate([
		{
			teacher: 'Alice' , year : 15,
			course : 'FDPC'  , grade: 10
		}, {
			teacher: 'Alice' , year : 14,
			course : 'IT'    , grade: 11
		}, {
			teacher: 'Bob'   , year : 16,
			course : 'PHYS'  , grade: 12
		}, {
			teacher: 'Bob'   , year : 14,
			course : 'CALC'  , grade: 12
		}, {
			teacher: 'Bob'   , year : 16,
			course : 'PC'    , grade: 11
		}
	])
})
.then(function() {
	console.log('Done: Classes\nEnrollments')
	for (var i = 0; i < 100; i++) {
		students.push({
			student: rand(300000, 200000),
			class: rand(6  , 1 ), // Max is exclusive - IDs are 1 to 5
			mark : rand(100, 50),
			exam : rand(100, 50)
		})
	}
	return db.Enrollment.bulkCreate(students)
})
.then(function() {
	console.log('Complete')
})
