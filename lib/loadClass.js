// Load a class by its ID to be viewed or modified

var db      = require('../lib/db'),
    cacheDB = require('../lib/cacheDB').db

require('../lib/titlecase') // .title()

module.exports = function(req, res, next) {
	// Don't overwrite a class if it's been retrieved already
	if (Object.keys(res.locals.data).length != 0) return next()

	var lastClass = req.session.lastClass,
		id = req.params.id || lastClass

	if (!id) return next()

	db.Class
		.findById(id, {
			include: [{
				model: db.Enrollment,
				as: 'classlist',
				attributes: { exclude: ['class'] }
			}]
		})
		.then(function(inst) {
			if (!inst) return next('Class ID not found')

			if (lastClass != id) {
				db.Teacher
					.findByPrimary(res.locals.name)
					.then(function(inst) {
						inst.update({ lastClass: id })
						req.session.lastClass = id
					})
			}

			res.locals.data = inst.get({ plain: true })
			return next()
		})
}
