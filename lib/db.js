// Database

var sqlize = require('sequelize'),
    config = require('../config'),
    bcrypt = require('bcryptjs')

require('./titlecase') // .title()

var db = new sqlize(config.db, {
	// logging: false,
	define: {
		timestamps: false
	},
	dialectOptions: {
		ssl: config.production
	}
})
console.log('Launching DB', config.db)

// Database schema

db.Teacher = db.define('Teacher', {
	name: {
		type: sqlize.STRING,
		primaryKey: true,
		set: function(val) {
			this.setDataValue('name', val.title())
		}
	},
	pass: {
		type: sqlize.STRING,
		allowNull: false,
		set: function(val) {
			if (!val) val = this.getDataValue('name')
			this.setDataValue('pass', bcrypt.hashSync(val, config.saltRounds))
		}
	},
	logins: {
		type: sqlize.INTEGER,
		defaultValue: 0
	},
	lastLogin: sqlize.DATE
	// lastClass: Fk for Class
})

db.Subject = db.define('Subject', {
	name: {
		type: sqlize.STRING,
		primaryKey: true,
		set: function(val) {
			this.setDataValue('name', val.title())
		}
	}
})

db.Course = db.define('Course', {
	id: {
		type: sqlize.STRING,
		primaryKey: true,
		set: function(val) {
			this.setDataValue('id', val.toUpperCase())
		}
	},
	name: {
		type: sqlize.STRING,
		unique: true,
		set: function(val) {
			this.setDataValue('name', val.title())
		}
	}
	// subject: Fk for Subject
})

db.Class = db.define('Class', {
	id: { // Used in /:type(view|edit) and as an FK for Enrollment
		type: sqlize.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
/*
	teacher: Fk for Teacher
	course:  Fk for Course
*/
	year: {
		type: sqlize.INTEGER,
		allowNull: false,
		validate: {
			notEmpty: true
			// Prevents the set function from turning '' into 2000
		},
		set: function(val) {
			val = val.toString().length < 4 ? 2000 + parseInt(val) : val
			this.setDataValue('year', val)
		}
	},
	grade: {
		type: sqlize.INTEGER,
		validate: { min: 9, max: 12 }
	}
})

db.Enrollment = db.define('Enrollment', {
	student: {
		type: sqlize.INTEGER,
		primaryKey: true,
		validate: { len: [6, 7] }
	},
	class: {
		type: sqlize.INTEGER,
		primaryKey: true
	},
	mark: {
		type: sqlize.INTEGER,
		validate: { min: 0, max: 100 }
	},
	exam: {
		type: sqlize.INTEGER,
		validate: { min: 0, max: 100 }
	}
})

// belongsTo adds the foreign key to the source model: Teacher
db.Teacher .belongsTo (db.Class  , { foreignKey : 'lastClass', constraints: false })

// Two-way relationship between Subject and Course
db.Subject .hasMany   (db.Course , {
	foreignKey: { name: 'subject', allowNull: false }
})
db.Course  .belongsTo (db.Subject, {
	foreignKey: { name: 'subject', allowNull: false }
})

// Two-way between Teacher and Class
// Note Class belongs to one row in Teacher, not many.
db.Teacher .hasMany   (db.Class  , {
	foreignKey: { name: 'teacher', allowNull: false }
})
db.Class   .belongsTo (db.Teacher, {
	foreignKey: { name: 'teacher', allowNull: false }
})

db.Course  .hasMany   (db.Class  , {
	foreignKey: { name: 'course' , allowNull: false }
})
db.Class   .belongsTo (db.Course , {
	foreignKey: { name: 'course' , allowNull: false }
})

db.Class.hasMany(db.Enrollment, { as: 'classlist', foreignKey: 'class' })

db.sync().catch(function(err) {
	throw('Sync error:', err)
})

process.on('SIGINT', function() {
	console.log('\nClosing DB') //'\n' is to avoid ^C
	db.close()
	process.exit(0)
})

module.exports = db
