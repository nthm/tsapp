// Error Handling

module.exports = function(err, req, res, next) {
	var knownHTTP = {
		403: 'Forbidden',
		404: 'Not Found',
		500: 'Internal Server Error'
	}

	var data = err

	if (knownHTTP.hasOwnProperty(data)) {
		var err = new Error(knownHTTP[data])
		err.status = data
	} else {
		var err = new Error(data)
	}

	err.status = err.status || 500

	res.status(err.status)
	res.format({
		html: function() {
			res.render('pages/error', {
				message: err.message,
				error: err
			})
		},

		json: function() {
			res.send({
				error: (err.message, err.status)
			})
		}
	})
}
