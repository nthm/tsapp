// Account page

var db     = require('../lib/db'),
    bcrypt = require('bcryptjs')

module.exports = function(req, res) {
	var data = res.locals.data

	if (req.method != 'POST') return res.render('account')

	var note = function(err, okay) {
		return res
			.status(err ? 417 : 200)
			.render('account', {
				note: { okay: !err, text: err || okay }
			})
	}

	var err = require('../lib/validate')(data)
	if (err) return note(err)

	db.Teacher
		.findByPrimary(res.locals.name, {
			//For 'name' https://github.com/sequelize/sequelize/issues/3953
			attributes: ['name', 'pass']
		})
		.then(function(inst) {
			bcrypt.compare(data.pass, inst.get('pass'), function(err, res) {
				if (!res)
					return note('Password is incorrect')

				inst
					.update({ pass: data.newPass })
					.then(function() {
						return note(null, 'Password updated')
					})
			})
		})
}
