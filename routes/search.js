// Search

var db      = require('../lib/db'),
    cache   = require('../lib/cacheDB'),
    cacheDB = cache.db,
    debug   = require('debug')('ts:search')

require('../lib/titlecase') // .title()

module.exports = function(req, res) {
	var query = res.locals.query = req.query.q

	var send = function(object) {
		res.render('results', object)
	}

	if (!query)
		return res.render('searchTerms')

	var queries = query.split(/, ?/),
	    queriesCopy = queries.slice(),
	    data = {}

	var store = function(key, term) {
		queriesCopy.splice(queriesCopy.indexOf(term), 1)

		// Convert term into a valid course ID
		if (key == 'course') {
			var courses = cacheDB.course,
			    upTerm  = term.toUpperCase()
			console.log(term)
			term = (function() {
				if (courses.hasOwnProperty(upTerm)) return upTerm
				for (var id in courses) if (courses[id] == term) return id
			})()
		}

		if (data.hasOwnProperty(key)) {
			if (data[key].constructor === Object)
				data[key]['$in'].push(term)
			else
				data[key] = { $in: [ data[key], term ] }
		} else {
			data[key] = term
		}
		debug(data)
	}

	// Indentify each term
	queries.forEach(function(term) {
		if (/^[a-zA-Z -]{2,30}$/.exec(term)) {
			term = term.title(); // Required semicolon
			['subject', 'course', 'teacher'].some(function(key) {
				if (cache.exists(term, key)) return store(key, term)
			})

		} else if (/^\d+$/.exec(term)) {
			var key = (function() {
				switch(term.length) {
					case 6: case 7: return 'student'
					case 4:         return 'year'
					case 2:         return 'grade'
				}
			})()
			if (key) return store(key, term)
		}
	})

	// Stop and report unknown terms
	if (queriesCopy.length > 0) {
		debug('Unknown terms:', queriesCopy)
		var x = list = queriesCopy
		for (var i in x) x[i] = '"' + x[i] + '"'

		if (x.length > 1) // Build a proper sentence
			list = x.slice(0,-1).join(', ') + ' and ' + x[x.length - 1]

		var word = x.length > 1 ? 'queries' : 'query'
		return send({
			info: 'Unknown ' + word + ': ' + list
		})
	}

	var includes = []
	if (data.hasOwnProperty('student')) {
		includes.push({
			model: db.Enrollment,
			as: 'classlist',
			attributes: { exclude: ['class'] },
			where: { student: data.student }
		})
		delete data.student
	}

	var courseInclude = { model: db.Course },
		courseData = {}
	if (data.hasOwnProperty('subject')) {
		courseData.subject = data.subject
		delete data.subject
	}
	if (data.hasOwnProperty('course')) {
		courseData.id = data.course
		delete data.course // Does this help efficiency?
	}

	if (Object.keys(courseData).length != 0)
		courseInclude.where = courseData

	includes.push(courseInclude)

	db.Class
		.findAll({
			where: data,
			attributes: { exclude: ['course'] },
			include: includes
		})
		.then(function(insts) {
			return send(insts.length ? {
				docs: JSON.parse(JSON.stringify(insts)) // For iteration in Jade
			} : {
				info: 'No matching classes'
			})
		})
}
