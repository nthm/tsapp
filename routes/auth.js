// Authentication

var db     = require('../lib/db'),
    config = require('../config'),
    debug  = require('debug')('ts:auth'),
    bcrypt = require('bcryptjs'),
    time   = require('strftime')

require('../lib/titlecase') // .title()

module.exports = function(req, res, next) {
	var data = res.locals.data

	var note = function(err) {
		res.render('pages/login', {
			error: err || 'Server Fault'
		})
	}
	
	var err = require('../lib/validate')(data)
	if (err) return note(err)

	var name = data.name.title(),
	    pass = data.pass

	debug('Authenticating', [name, pass].join(':'))

	db.Teacher
		.findByPrimary(name)
		.then(function(inst) {
			if (!inst)
				return note('Not found')

			var hash = inst.get('pass')
			if (hash == null)
				return note('Deactivated')

			var now = new Date()
			inst.update({ lastLogin: now })

			bcrypt.compare(pass, hash, function(err, result) {
				if (!result)
					return note('Incorrect password')

				inst.increment('logins')

				res.cookie('auth', {
					name: name,
					time: time('%-I:%M %p %b %-e', now)
				}, {
					maxAge: 1000 * 3600 * 24 * 7, // 1 week
					signed: true
				})

				var toPush = {
					name: name,
					lastClass: inst.get('lastClass'),
					showInfo : inst.get('logins') < config.showInfo
				}

				for (var field in toPush)
					req.session[field] = toPush[field]

				res.redirect('back')
			})
		})
}
