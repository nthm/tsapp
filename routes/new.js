// Insert data to the system

var db      = require('../lib/db'),
    cache   = require('../lib/cacheDB'),
    cacheDB = cache.db,
    debug   = require('debug')('ts:new')

require('../lib/titlecase') // .title()

module.exports = function(req, res, next) {
	var type = res.locals.edit ? 'Class' : req.params.type.title(),
		data = res.locals.data

	// Store the type before it is modified
	res.locals.type = type

	var note = function(err, okay) {
		res.locals.note = { okay: !err, text: err || okay }
		send(err)
	}

	var send = function(err) {
		return res
			.status(err ? 417 : 200)
			.render('new/new')
	}

	if (req.method != 'POST') return send()

	var sequelizeError = function(err) {
		switch (err.name) {
			case 'SequelizeForeignKeyConstraintError':
				return note('Prerequisite does not exist')

			case 'SequelizeValidationError':
				return note('Invalid field')
		}
		return next(err)
	}

	var err = require('../lib/validate')(data)
	if (err) return note(err)

	if (type != 'Class') {
		db[type]
			.findOrCreate({
				where: { name: data.name }, // All models use the 'name' field
				defaults: data
			})
			.spread(function(inst, created) {
				if (!created) return note(type + ' already exists')
				type = type.toLowerCase()
				cache.insert(type == 'course' ? data : data.name, type)

				return note(null, 'Created')
			})
			.catch(sequelizeError)
	} else {
		var integradeRegex = '([0-9]{6,7})\\s+([0-9]{2})\\s+([0-9]{2})',
		    regex = new RegExp(data.regex || integradeRegex, 'gm'),
		    classlist = []

		var m // Match
		while (m = regex.exec(data.classlist)) {
			if (m.length != 4) continue
			classlist.push({ student: m[1], mark: m[2], exam: m[3] })
		}

		if (!classlist.length)
			return note(regex + ' matched no lines');

		var id
		// Decide if a class needs to be created or updated
		(function() {
			if (!res.locals.edit)
				return db.Class.create(data)

			return db.Class
				.findById(req.params.id)
				.then(function(inst) { return inst.update(data) })
		})()
			.then(function(inst) { id = inst.get('id') })
			.then(function() {
				// Drop all students from the class if editing
				if (res.locals.edit)
					return db.Enrollment.destroy({ where: { class: id } })
			})
			.then(function() {
				// Add the class field to each student before creation
				for (var row of classlist) row.class = id
				return db.Enrollment.bulkCreate(classlist)
			})
			.then(function() { res.redirect('/view/' + id) })
			.catch(sequelizeError)
	}
}
