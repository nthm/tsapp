# Transcripts

An Express web application for managing student grades.

_Archived. Last updated 2016_

Demo: https://tsapp-demo.herokuapp.com

![](./demo.gif)

## Features

- Authentication
- Manages classes, courses, teachers, and subjects
- Support for classlist data exported from InteGrade Pro
- Search
- Uses [Sequelize](http://docs.sequelizejs.com/) to support several database
  backends

## Missing features

The project is now archived, but these are features that should be considered:

- Authorization
- Ability to delete or edit courses, teachers, and subjects
- Fuzzy search. Currently search is exact match only
- No frontend; all code is server side in Express. A JS frontend (Preact?)
  should be created and the server should be rewritten as mostly an API

## Installation

Clone the repo and install modules with `npm install`. You can connect to the
database by modifying `config.js` or using environmental variables (see below
for details). The system can be populated with example data using `node
./lib/initDB` which will provide an account to order to login.

While only tested with PostgreSQL in production, the application should work
with MySQL and SQLite without any modifications.

```js
$ node
> var db = require('./lib/db')
Launching DB ...
> db.Teacher.create({ name: 'Dev', pass: ''})
```

Launch the application with `npm run start`

## Configuration

Environmental variables:
- DATABASE_URL
- NODE_ENV (used to set SSL for PostgreSQL)
- DEBUG ts:\*

Options in `config.js`:
- noAuth: allow users with a signed cookie to bypass the login page
- saltRounds: used by bcrypt.js
- showInfo: number of logins required before the "How To" is hidden

## Development

This project was built to run entirely server side as an Express application
with rendering via Jade (now called Pug). This was done entirely as a practice
and introduction into backend web developement. Today, 2 years later, as I touch
up this README, I'd recommend a purely frontend application to avoid setting up
a local database on campus.

PS. This underwent an intensive rebase to cleanup the codebase on July 1st 2018