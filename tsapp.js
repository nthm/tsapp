// Transcripts
var express = require('express'),
    app = express()

var config = require('./config')

var bParser = require('body-parser')
app.use(bParser.text())
app.use(bParser.json())
app.use(bParser.urlencoded({ extended: false }))

var cParser = require('cookie-parser')
app.use(cParser(config.keys.cookie))

var icon = require('serve-favicon')
app.use(icon(__dirname + '/public/img/favicon.ico'))

app.set('view engine', 'jade')
app.set('views', __dirname + '/views')

var stylus = require('stylus')
app.use(stylus.middleware({
	src : __dirname + '/lib/stylus',
	dest: __dirname + '/public'
}))

app.use(express.static(__dirname + '/public'))

app.use(require('morgan')('dev'))

var session = require('express-session')
app.use(session({
	resave: false,
	saveUninitialized: false,
	secret: config.keys.session
}))

var db        = require('./lib/db'),
    cache     = require('./lib/cacheDB'),
    loadClass = require('./lib/loadClass')

// Load the .title() prototype
require('./lib/titlecase')

app.use(function(req, res, next) {
	res.locals.data = req.body
	return next()
})
app.route('/auth')
	.get  (function(req, res) { res.redirect('/') })
	.post (require('./routes/auth'))

app.use(function(req, res, next) {
	var client = req.session,
	    cookie = req.signedCookies.auth

	if (config.noAuth && cookie)
		client.name = cookie.name

	if (client.name) {
		res.locals.name = client.name
		res.locals.cacheDB = cache.db
		return next()
	}

	res.status(401).render('pages/login', cookie)
})
app.get('/',
	loadClass,
	function(req, res) {
		res.locals.showInfo = req.session.showInfo
		if (Object.keys(res.locals.data).length != 0)
			return res.render('class')
		res.render('searchTerms')
	}
)
app.get('/search',
	require('./routes/search')
)
app.all('/account',
	require('./routes/account')
)
app.all('/new/:type(subject|teacher|course|class)',
	require('./routes/new')
)
app.all('/edit/:id([0-9]+)',
	loadClass,
	function(req, res, next) {
		res.locals.edit = true
		return next()
	},
	require('./routes/new')
)
app.get('/view/:id([0-9]+)',
	loadClass,
	function(req, res) { res.render('class') }
)
app.get('/delete/:id([0-9]+)',
	function(req, res) {
		db.Class.destroy({ where: { id: req.params.id } })
		res.redirect('/')
	}
)
app.get('/logout',
	function(req, res) {
		if (config.noAuth) res.clearCookie('auth')
		req.session.destroy()
		res.redirect('/')
	}
)
app.use(function(req, res, next) {
	return next(404)
})
app.use(require('./lib/errorHandler'))

// Load cache
cache.init(function() {
	var port = process.env.PORT || 3000

	app.listen(port)
	console.log('Listening on port', port)
})
